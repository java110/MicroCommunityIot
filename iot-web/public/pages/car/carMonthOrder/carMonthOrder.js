/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            carMonthOrderInfo: {
                carMonthOrders: [],
                primeRates:[],
                cards:[],
                total: 0,
                records: 1,
                moreCondition: false,
                orderId: '',
                conditions: {
                    orderId: '',
                    cardId: '',
                    carNum: '',
                    primeRate: '',
                    state: '',
                    queryStartTime:'',
                    queryEndTime:'',
                }
            }
        },
        _initMethod: function () {
            $that._listCarMonthOrders(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listMonthCards();
            vc.getDict('pay_fee_detail', "prime_rate", function(_data) {
                $that.carMonthOrderInfo.primeRates = _data;
            });

            vc.initDateTime('queryStartTime',function(_value){
                $that.carMonthOrderInfo.conditions.queryStartTime = _value;
            });
            vc.initDateTime('queryEndTime',function(_value){
                $that.carMonthOrderInfo.conditions.queryEndTime = _value;
            });
        },
        _initEvent: function () {

            vc.on('carMonthOrder', 'listCarMonthOrder', function (_param) {
                $that._listCarMonthOrders(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                $that._listCarMonthOrders(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listCarMonthOrders: function (_page, _rows) {

                $that.carMonthOrderInfo.conditions.page = _page;
                $that.carMonthOrderInfo.conditions.row = _rows;
                $that.carMonthOrderInfo.conditions.communityId = vc.getCurrentCommunity().communityId;
                let param = {
                    params: $that.carMonthOrderInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/carMonth.listCarMonthOrder',
                    param,
                    function (json, res) {
                        let _json = JSON.parse(json);
                        $that.carMonthOrderInfo.total = _json.total;
                        $that.carMonthOrderInfo.records = _json.records;
                        $that.carMonthOrderInfo.carMonthOrders = _json.data;
                        vc.emit('pagination', 'init', {
                            total: $that.carMonthOrderInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openBuyMonthCardModal: function () {
                vc.emit('buyCarMonthCard', 'openBuyCarMonthCardModal', {});
            },
            _openQrcodeBuyModal:function(){
                vc.emit('buyMonthCardQrCode', 'openQrCodeModal',{});
            },
            _openRetundModel: function (_carMonthOrder) {
                vc.emit('retundCarMonthOrder','openRetundOrderModal', _carMonthOrder);
            },
            _queryCarMonthOrderMethod: function () {
                $that._listCarMonthOrders(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if ($that.carMonthOrderInfo.moreCondition) {
                    $that.carMonthOrderInfo.moreCondition = false;
                } else {
                    $that.carMonthOrderInfo.moreCondition = true;
                }
            },
            _listMonthCards: function () {

                let param = {
                    params:{
                        page:1,
                        row:100,
                        communityId:vc.getCurrentCommunity().communityId
                    }
                };

                //发送get请求
                vc.http.apiGet('/carMonth.listCarMonthCard',
                    param,
                    function (json, res) {
                        let _json = JSON.parse(json);
                        $that.carMonthOrderInfo.cards = _json.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
        }
    });
})(window.vc);
