package com.java110.charge.factory.sbcharge.exception;

public class InvalidKeyTypeException extends RuntimeException {
    public InvalidKeyTypeException(String message) {
        super(message);
    }
}
