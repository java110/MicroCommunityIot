package com.java110.charge.factory.sbcharge.exception;

public class PackKVFailException extends RuntimeException {
    public PackKVFailException(String message) {
        super(message);
    }
}
