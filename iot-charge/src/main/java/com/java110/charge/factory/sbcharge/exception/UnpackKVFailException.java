package com.java110.charge.factory.sbcharge.exception;

public class UnpackKVFailException extends RuntimeException {
    public UnpackKVFailException(String message) {
        super(message);
    }
}
