package com.java110.charge;

/**
 * 充电桩服务启动类
 * spring cloud 方式启动时 所有的 请求都进这个服务，其他微服务不对外提供服务
 * <p>
 * 主要用于 网关鉴权 等功能
 * <p>
 * add by wuxw 2023-02-10
 */

public class ChargeApplicationStart {


}
