package com.java110.meter.factory.common;

import com.java110.bean.ResultVo;
import com.java110.dto.meter.MeterMachineDto;
import com.java110.meter.factory.ISmartMeterFactoryAdapt;
import org.springframework.stereotype.Service;

import java.util.List;

/*
   通用 电表接口类

   推荐其他智能电表实现
 */
@Service("commonFactoryAdaptImpl")
public class CommonFactoryAdaptImpl implements ISmartMeterFactoryAdapt {
    @Override
    public ResultVo requestRecharge(MeterMachineDto meterMachineDto, double degree, double money) {
        return null;
    }

    @Override
    public ResultVo requestRead(MeterMachineDto meterMachineDto) {
        return null;
    }

    @Override
    public ResultVo requestReads(List<MeterMachineDto> meterMachineDtos) {
        return null;
    }

    @Override
    public ResultVo notifyReadData(String readData) {
        return null;
    }

    @Override
    public void queryMeterMachineState(MeterMachineDto meterMachineDto) {
        return;
    }

    @Override
    public ResultVo switchControl(MeterMachineDto meterMachineDto, String controlType) {
        return null;
    }

    @Override
    public ResultVo cleanControl(MeterMachineDto meterMachineDto) {
        return null;
    }
}
