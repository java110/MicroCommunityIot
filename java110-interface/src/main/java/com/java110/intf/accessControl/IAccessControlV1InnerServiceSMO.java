/*
 * Copyright 2017-2020 吴学文 and java110 team.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.java110.intf.accessControl;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.accessControl.AccessControlDto;
import com.java110.dto.accessControl.MachineHeartbeatDto;
import com.java110.dto.chargeMachine.ChargeMachineOrderDto;
import com.java110.intf.FeignConfiguration;
import com.java110.po.accessControl.AccessControlPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

/**
 * 类表述： 服务之前调用的接口类，不对外提供接口能力 只用于接口建调用
 * add by 吴学文 at 2023-08-15 02:16:27 mail: 928255095@qq.com
 * open source address: https://gitee.com/wuxw7/MicroCommunity
 * 官网：http://www.homecommunity.cn
 * 温馨提示：如果您对此文件进行修改 请不要删除原有作者及注释信息，请补充您的 修改的原因以及联系邮箱如下
 * // modify by 张三 at 2021-09-12 第10行在某种场景下存在某种bug 需要修复，注释10至20行 加入 20行至30行
 */
@FeignClient(name = "iot-accessControl", configuration = {FeignConfiguration.class})
@RequestMapping("/accessControlV1Api")
public interface IAccessControlV1InnerServiceSMO {


    @RequestMapping(value = "/saveAccessControl", method = RequestMethod.POST)
    public int saveAccessControl(@RequestBody AccessControlPo accessControlPo);

    @RequestMapping(value = "/updateAccessControl", method = RequestMethod.POST)
    public int updateAccessControl(@RequestBody AccessControlPo accessControlPo);

    @RequestMapping(value = "/deleteAccessControl", method = RequestMethod.POST)
    public int deleteAccessControl(@RequestBody AccessControlPo accessControlPo);

    /**
     * <p>查询小区楼信息</p>
     *
     * @param accessControlDto 数据对象分享
     * @return AccessControlDto 对象数据
     */
    @RequestMapping(value = "/queryAccessControls", method = RequestMethod.POST)
    List<AccessControlDto> queryAccessControls(@RequestBody AccessControlDto accessControlDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param accessControlDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryAccessControlsCount", method = RequestMethod.POST)
    int queryAccessControlsCount(@RequestBody AccessControlDto accessControlDto);

    /**
     * 门禁心跳
     *
     * @param machineHeartbeatDto 心跳信息
     * @return
     */
    @RequestMapping(value = "/accessControlHeartbeat", method = RequestMethod.POST)
    int accessControlHeartbeat(@RequestBody MachineHeartbeatDto machineHeartbeatDto);

    @RequestMapping(value = "/queryAcInoutStatistics", method = RequestMethod.POST)
    List<Map> queryAcInoutStatistics(@RequestBody JSONObject reqJson);

    @RequestMapping(value = "/queryAcLogStatistics", method = RequestMethod.POST)
    List<Map> queryAcLogStatistics(@RequestBody JSONObject reqJson);

    @RequestMapping(value = "/queryAcVisitStatistics", method = RequestMethod.POST)
    List<Map> queryAcVisitStatistics(@RequestBody JSONObject reqJson);

    @RequestMapping(value = "/queryAcFaceStatistics", method = RequestMethod.POST)
    List<Map> queryAcFaceStatistics(@RequestBody JSONObject reqJson);

    @RequestMapping(value = "/queryOwnerAccessControlsCount", method = RequestMethod.POST)
    int queryOwnerAccessControlsCount(@RequestBody AccessControlDto accessControlDto);

    @RequestMapping(value = "/queryOwnerAccessControls", method = RequestMethod.POST)
    List<AccessControlDto> queryOwnerAccessControls(@RequestBody AccessControlDto accessControlDto);


    @RequestMapping(value = "/openDoor", method = RequestMethod.POST)
    int openDoor(@RequestBody AccessControlDto accessControlDto);
}
