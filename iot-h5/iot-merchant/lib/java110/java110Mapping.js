/**
 * 编码映射常量类
 *
 * add by wuxw 2019-12-28
 */
export default {
	OWNER_KEY: "owner_key", // 业主临时key
	AREA_INFO: "areaInfo", // 地区信息
	// CURRENT_COMMUNITY_INFO: "currentCommunityInfo", // 小区信息
	OWNER_INFO: "ownerInfo", // 当前业主信息
	CURRENT_COMMUNITY_INFO: "currentCommunityInfo", //业主当前小区信息
	CURRENT_SHOP_INFO: "currentShopInfo", //业主当前店铺信息
	CURRENT_OPEN_ID: "openId",
	USER_INFO: "HC_MALL_USER_INFO", // 用户信息
	COMMUNITY: "HC_MALL_COMMUNITY",
	MALL_FROM: "HC_MALL_FROM",
	MALL_FROM_APP:"APP", // 来自于app
	MALL_FROM_WECHAT:"WECHAT", //来自于公众号
	MALL_FROM_MINI:"MINI", //来自于小程序
	MALL_SHOP_ID:"MALL_SHOP_ID",
	LOGIN_FLAG: "HC_MALL_LOGIN_FLAG",
	TOKEN: "HC_MALL_TOKEN",
	W_APP_ID: "wAppId", //微信公众号ID
	HOMEMAKING_CATEGORY_LIST: "HOMEMAKING_CATEGORY_TYPE_LIST", //家政服务菜单（类型）
	CACHE_CURRENT_SERV: "CACHE_CURRENT_SERV_DETAIL", // 当前家政服务详情数据(列表页点击跳转的时候缓存家政服务详情)
}
