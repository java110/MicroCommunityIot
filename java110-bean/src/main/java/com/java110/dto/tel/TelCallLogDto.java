package com.java110.dto.tel;

import com.java110.bean.dto.PageDto;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 通话记录数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class TelCallLogDto extends PageDto implements Serializable {

    private String personName;
private String talkTime;
private String machineId;
private String ccid;
private String voicePath;
private String clId;
private String tel;
private String personId;
private String state;
private String communityId;
private String callType;


    private Date createTime;

    private String statusCd = "0";


    public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getTalkTime() {
        return talkTime;
    }
public void setTalkTime(String talkTime) {
        this.talkTime = talkTime;
    }
public String getMachineId() {
        return machineId;
    }
public void setMachineId(String machineId) {
        this.machineId = machineId;
    }
public String getCcid() {
        return ccid;
    }
public void setCcid(String ccid) {
        this.ccid = ccid;
    }
public String getVoicePath() {
        return voicePath;
    }
public void setVoicePath(String voicePath) {
        this.voicePath = voicePath;
    }
public String getClId() {
        return clId;
    }
public void setClId(String clId) {
        this.clId = clId;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getPersonId() {
        return personId;
    }
public void setPersonId(String personId) {
        this.personId = personId;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getCommunityId() {
        return communityId;
    }
public void setCommunityId(String communityId) {
        this.communityId = communityId;
    }
public String getCallType() {
        return callType;
    }
public void setCallType(String callType) {
        this.callType = callType;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
