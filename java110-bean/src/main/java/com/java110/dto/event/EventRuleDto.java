package com.java110.dto.event;

import com.java110.bean.dto.PageDto;

import java.io.Serializable;
import java.util.Date;

public class EventRuleDto extends PageDto implements Serializable {
    private String ruleId;
    private String ruleName;
    private Integer limitTime;
    private String templateId;
    private String communityId;
    private String remark;
    private Date createTime;
    private String statusCd = "0";
    private EventTemplateDto eventTemplate;

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getCommunityId() {
        return communityId;
    }

    public void setCommunityId(String communityId) {
        this.communityId = communityId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public Integer getLimitTime() {
        return limitTime;
    }

    public void setLimitTime(Integer limitTime) {
        this.limitTime = limitTime;
    }

    public EventTemplateDto getEventTemplate() {
        return eventTemplate;
    }

    public void setEventTemplate(EventTemplateDto eventTemplate) {
        this.eventTemplate = eventTemplate;
    }
}
