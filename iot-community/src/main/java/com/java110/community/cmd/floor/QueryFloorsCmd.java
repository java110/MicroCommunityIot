package com.java110.community.cmd.floor;

import com.alibaba.fastjson.JSONObject;
import com.java110.bean.ResultVo;
import com.java110.bean.dto.floor.FloorDto;
import com.java110.bean.dto.room.RoomDto;
import com.java110.bean.dto.unit.UnitDto;
import com.java110.core.annotation.Java110Cmd;
import com.java110.core.cmd.Cmd;
import com.java110.core.cmd.CmdEvent;
import com.java110.core.context.ICmdDataFlowContext;
import com.java110.core.exception.CmdException;
import com.java110.core.utils.Assert;
import com.java110.core.utils.BeanConvertUtil;
import com.java110.core.utils.ListUtil;
import com.java110.doc.annotation.*;
import com.java110.intf.community.IFloorInnerServiceSMO;
import com.java110.intf.community.IRoomV1InnerServiceSMO;
import com.java110.intf.community.IUnitV1InnerServiceSMO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;


@Java110CmdDoc(title = "查询楼栋",
        description = "用于外系统查询楼栋信息功能",
        httpMethod = "get",
        url = "http://{ip}:{port}/iot/api/floor.queryFloors",
        resource = "communityDoc",
        author = "吴学文",
        serviceCode = "floor.queryFloors"
)

@Java110ParamsDoc(params = {
        @Java110ParamDoc(name = "communityId", length = 30, remark = "小区ID"),
        @Java110ParamDoc(name = "page", type = "int", length = 11, remark = "页数"),
        @Java110ParamDoc(name = "row", type = "int", length = 11, remark = "行数"),
})

@Java110ResponseDoc(
        params = {
                @Java110ParamDoc(name = "code", type = "int", length = 11, defaultValue = "0", remark = "返回编号，0 成功 其他失败"),
                @Java110ParamDoc(name = "msg", type = "String", length = 250, defaultValue = "成功", remark = "描述"),
                @Java110ParamDoc(name = "apiFloorDataVoList", type = "Array", remark = "有效数据"),
                @Java110ParamDoc(parentNodeName = "apiFloorDataVoList", name = "floorId", type = "String", remark = "楼栋ID"),
                @Java110ParamDoc(parentNodeName = "apiFloorDataVoList", name = "floorNum", type = "String", remark = "楼栋编号"),
        }
)

@Java110ExampleDoc(
        reqBody = "http://{ip}:{port}/app/floor.queryFloors?page=1&row=10&communityId=123123",
        resBody = "{'code':0,'msg':'成功','apiFloorDataVoList':[{'floorId':'123123','floorNum':'123213'}]}"
)

@Java110Cmd(serviceCode = "floor.queryFloors")
public class QueryFloorsCmd extends Cmd {

    @Autowired
    private IFloorInnerServiceSMO floorInnerServiceSMOImpl;

    @Autowired
    private IRoomV1InnerServiceSMO roomV1InnerServiceSMOImpl;

    @Autowired
    private IUnitV1InnerServiceSMO unitV1InnerServiceSMOImpl;

    @Override
    public void validate(CmdEvent event, ICmdDataFlowContext cmdDataFlowContext, JSONObject reqJson) {
        Assert.jsonObjectHaveKey(reqJson, "page", "请求中未包含page信息");
        Assert.jsonObjectHaveKey(reqJson, "row", "请求中未包含row信息");
        Assert.jsonObjectHaveKey(reqJson, "communityId", "请求中未包含communityId信息");
        Assert.isInteger(reqJson.getString("page"), "page不是有效数字");
        Assert.isInteger(reqJson.getString("row"), "row不是有效数字");
    }

    @Override
    public void doCmd(CmdEvent event, ICmdDataFlowContext cmdDataFlowContext, JSONObject reqJson) throws CmdException {
        //int page = reqJson.getInteger("page");
        int row = reqJson.getInteger("row");
        //String communityId = reqJson.getString("communityId");

        List<FloorDto> floorDtos = null;
        //查询总记录数
        int total = floorInnerServiceSMOImpl.queryFloorsCount(BeanConvertUtil.covertBean(reqJson, FloorDto.class));
        if (total > 0) {
            floorDtos = floorInnerServiceSMOImpl.queryFloors(BeanConvertUtil.covertBean(reqJson, FloorDto.class));
        }

        //todo 查询房屋数
        computeRoomCount(floorDtos);

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) total / (double) reqJson.getInteger("row")), total, floorDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        cmdDataFlowContext.setResponseEntity(responseEntity);
    }

    private void computeRoomCount(List<FloorDto> floorDtos) {

        if (ListUtil.isNull(floorDtos)) {
            return;
        }

        if (floorDtos.size() != 1) {
            return;
        }

        RoomDto roomDto = new RoomDto();
        roomDto.setFloorId(floorDtos.get(0).getFloorId());
        roomDto.setCommunityId(floorDtos.get(0).getCommunityId());
        int roomCount = roomV1InnerServiceSMOImpl.queryRoomsCount(roomDto);

        floorDtos.get(0).setRoomCount(roomCount);

        roomDto = new RoomDto();
        roomDto.setFloorId(floorDtos.get(0).getFloorId());
        roomDto.setCommunityId(floorDtos.get(0).getCommunityId());
        roomDto.setState(RoomDto.STATE_FREE);
        int feeRoomCount = roomV1InnerServiceSMOImpl.queryRoomsCount(roomDto);

        floorDtos.get(0).setFreeRoomCount(feeRoomCount);

        UnitDto unitDto = new UnitDto();
        unitDto.setFloorId(floorDtos.get(0).getFloorId());
        unitDto.setCommunityId(floorDtos.get(0).getCommunityId());
        List<UnitDto> unitDtos = unitV1InnerServiceSMOImpl.queryUnits(unitDto);

        if (ListUtil.isNull(unitDtos)) {
            floorDtos.get(0).setLayer(1);
            return;
        }

        floorDtos.get(0).setLayer(Integer.parseInt(unitDtos.get(0).getLayerCount()));
    }
}
